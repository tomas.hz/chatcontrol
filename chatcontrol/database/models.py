import datetime

import sqlalchemy

from . import Base
from .utils import UUID, get_uuid

__all__ = ["Signature"]


class Signature(Base):
	__tablename__ = "signatures"

	id = sqlalchemy.Column(
		UUID,
		primary_key=True,
		default=get_uuid
	)

	name = sqlalchemy.Column(
		sqlalchemy.String(64),
		nullable=False
	)
	surname = sqlalchemy.Column(
		sqlalchemy.String(64),
		nullable=False
	)
	
	city = sqlalchemy.Column(
		sqlalchemy.String(64),
		nullable=False
	)
	country = sqlalchemy.Column(
		sqlalchemy.String(3),
		nullable=True
	)
	
	email = sqlalchemy.Column(
		sqlalchemy.String(128),
		nullable=True
	)
	comment = sqlalchemy.Column(
		sqlalchemy.String(65536),
		nullable=True
	)
