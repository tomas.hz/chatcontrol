import flask
import flask_wtf
import pycountry
import sqlalchemy
import wtforms

from .. import database


web_blueprint = flask.Blueprint(
	"web",
	__name__
)


class CountrySelectField(wtforms.SelectField):
	# https://gist.github.com/mekza/516f172278c328468ea0
	# Thanks to mekza!
	def __init__(self, *args, **kwargs):
		super(CountrySelectField, self).__init__(*args, **kwargs)
		
		self.choices = [
			(country.alpha_2, country.name)
			for country
			in pycountry.countries
		]


class SignatureForm(flask_wtf.FlaskForm):
	name = wtforms.StringField(
		validators=[
			wtforms.validators.InputRequired(),
			wtforms.validators.Length(max=64)
		]
	)
	surname = wtforms.StringField(
		validators=[
			wtforms.validators.InputRequired(),
			wtforms.validators.Length(max=64)
		]
	)
	
	city = wtforms.StringField(
		validators=[
			wtforms.validators.InputRequired(),
			wtforms.validators.Length(max=64)
		]
	)
	country = CountrySelectField(
		validators=[
			wtforms.validators.Length(max=3)
		]
	)
	
	email = wtforms.EmailField(
		validators=[wtforms.validators.Length(max=128)]
	)
	comment = wtforms.TextAreaField(
		validators=[wtforms.validators.Length(max=65536)]
	)


@web_blueprint.route("/", methods=["GET"])
def view_index():
	return flask.render_template("index.html", is_article=True)


@web_blueprint.route("/materials", methods=["GET"])
def view_materials():
	return flask.render_template("materials.html", is_article=False)

@web_blueprint.route("/petition", methods=["GET", "POST"])
@flask.current_app.limiter.limit(
	"29/day",
	exempt_when=lambda: flask.request.method != "POST"
)
def view_petition():
	form = SignatureForm()
	
	added = False
	
	if flask.request.method == "POST" and form.validate_on_submit():
		signature = database.Signature(
			name=form.name.data,
			surname=form.surname.data,
			city=form.city.data,
			country=form.country.data,
			email=form.email.data,
			comment=form.comment.data
		)
		
		flask.g.sa_session.add(signature)
		flask.g.sa_session.commit()
		
		added = True
		
	signature_count = flask.g.sa_session.execute(
		sqlalchemy.select(sqlalchemy.func.count(database.Signature.id))
	).scalars().one()
	
	return flask.render_template(
		"petition.html",
		form=form,
		added=added,
		signature_count=signature_count,
		is_article=False
	)


@web_blueprint.route("/gdpr", methods=["GET"])
def view_gdpr():
	return flask.render_template("gdpr.html", is_article=False)
